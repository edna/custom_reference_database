import pathlib
from setuptools import find_packages, setup
from mkbdr import __version__


# The directory containing this file
HERE = pathlib.Path(__file__).parent

README = (HERE / "README.md").read_text()


setup(
    name='mkbdr',
    version=__version__,
    author="Pierre-Edouard GUERIN",    
    author_email="pierre-edouard.guerin@cefe.cnrs.fr",
    description="A toolkit to build a custom metabarcoding reference database.",
    long_description=README,
    long_description_content_type="text/markdown",
    license="MIT",
    url="https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database",   
    packages=find_packages(),
    install_requires=['argparse', 'numpy', 'biopython', 'pandas', 'PyQt5', 'ete3', 'pytaxize', 'pathlib'],
    classifiers=[
         "Programming Language :: Python :: 3",
         "Programming Language :: Python :: 3.6",
         "Programming Language :: Python :: 3.7",
         "Programming Language :: Python :: 3.8",
         "Programming Language :: Python :: 3.9",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent"
    ],
    entry_points={
    'console_scripts': [
        'mkbdr=mkbdr.__main__:main',
    ],
},  
)