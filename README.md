# Custom Metabarcoding Reference Database

[![Twitter Follow](https://img.shields.io/twitter/follow/ephe_bev?style=social)](https://twitter.com/ephe_bev)


## Introduction

**MKBDR** is a python program designed to create custom reference database from FASTA file using the NCBI taxonomy. It also provides tools to assist and perform taxonomy curation on the input FASTA file.



## Method

1. [Installation](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Installing-MKBDR)
2. [Input Files](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Files-definition#input-files)
    * [species representative records FASTA file](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Files-definition#representative-sequences)
    * [NCBI taxonomy files](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Files-definition#ncbi-taxonomies-file)
    * [Curation table file](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Files-definition#curation-file)
3. [Quick start](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/home#quick-start)
4. [Running MKBDR](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Running-MKBDR)
    * [Module validate - Check taxonomy and format validity](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Validate)
    * [Module cureGen - Curation Generation](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Curegen)
    * [Module init_ncbi_taxdump - Download NCBI taxonomy](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/init_ncbi_taxdump)
    * [Apply taxonomy curation](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Validate#basic-curation)
    * [Add new species to NCBI taxonomy](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Validate#using-a-local-ncbi-taxonomy-performs-a-curation-which-add-new-species-to-your-local-taxonomy)
4. [Output results](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/Files-definition#output-files)
5. [How-to guide](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/How-to-guide)
6. [Reference](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/wikis/home#credits)
7. [Metabarcoding context - discussion to go further]()


![mkbdr](docs/mkbdr.png)


## Credits

**MKBDR** was coded and written by Pierre-Edouard Guerin, Laetitia Mathon and Virginie Marques.

We thank the following people for their help in the development of this software: Virginie Marques, Alice Valentini, David Mouillot, Emilie Boulanger, Laetitia Mathon, Laura Benestan, Stephanie Manel, Tony Dejean.



## Contributions and Support

:bug: If you are sure you have found a bug, please submit a bug report. You can submit your bug reports on Gitlab [here](https://gitlab.mbb.univ-montp2.fr/edna/custom_reference_database/-/issues).




For further information or help, don't hesitate to get in touch on Slack (you can join with this invite).

[![Get help on Slack](https://img.shields.io/badge/slack-cefebev%23basereference-4A154B?logo=slack)](https://cefebev.slack.com/archives/C01MDQSS57F)



