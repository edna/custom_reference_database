## Standard library imports
import sys
import os
import re
import csv
import pickle
import unittest
from unittest.mock import Mock
## Local applications import
## external
from ete3 import Tree, TreeStyle, NodeStyle, TextFace, NCBITaxa
import numpy as np
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
## local
from mkbdr.objects import Sample, DescriptionRecord
from mkbdr.validate import check_record_description, check_format_species_name, check_dna_sequence, check_taxonomy_species_name



class ValidateTest(unittest.TestCase):
    """
    test case to check functions from the module validate
    """      
    
    def test_check_record_description(self):
        """
        test case function check_record_description        
        """
        record_description = ""
        assert check_record_description(record_description) is False
        record_description = "ID  nimportequoi"
        assert check_record_description(record_description) is False
        record_description = "ID ; nimportequoi"
        assert check_record_description(record_description) is False
        record_description = "ID ; species_name:nimportequoi"
        assert check_record_description(record_description) is False
        record_description = "ID ; genus=nimp; species_name=nimportequoi"
        assert check_record_description(record_description) is False
        record_description = "ID ; species=Mullus surmuletus"
        assert check_record_description(record_description) is False
        record_description = "ID1; species_name=Mullus surmuletus"
        descriptionRecord = DescriptionRecord(sampleid='ID1', species_name='Mullus surmuletus')
        assert check_record_description(record_description) == descriptionRecord
    
    def test_check_format_species_name(self):
        """
        test case function check_format_species_name       
        """
        goodSpeciesName = 'Mullus surmuletus'
        assert check_format_species_name(goodSpeciesName) == goodSpeciesName
        speciesName = 'Mullus_surmuletus'
        assert check_format_species_name(speciesName) == goodSpeciesName
        speciesName = 'Mullus surmuletus sp'
        assert check_format_species_name(speciesName) is False
        speciesName = 'Mullus'
        assert check_format_species_name(speciesName) is False
        speciesName = ''
        assert check_format_species_name(speciesName) is False
        speciesName = 'Mullus sp.'
        assert check_format_species_name(speciesName) is False
        speciesName = 'Mullus sp Cuillere'
        assert check_format_species_name(speciesName) is False

    def test_check_dna_sequence(self):
        """
        test case function check_dna_sequence
        """
        wrongSequence="CATAGC-GCG--"
        assert check_dna_sequence(wrongSequence) is False
        wrongSequence="Carrement nimp"
        assert check_dna_sequence(wrongSequence) is False
        wrongSequence="CATAGCNGCG"
        assert check_dna_sequence(wrongSequence) is False
        goodSequence="CATAGCGAT"
        assert check_dna_sequence(goodSequence) is True

    def test_check_taxonomy_species_name(self):
        """
        test case function check_taxonomy_species_name
        """
        assert True is True
