## Standard library imports
import pandas
import sys
import itertools
import copy
import os
import re
import csv
import pickle
import unittest
from unittest.mock import Mock

## Third party imports
from ete3 import Tree, TreeStyle, NodeStyle, TextFace, NCBITaxa
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

## Local applications import
from mkbdr.objects import GenID, Sample, TaxonNode
from mkbdr.curate import is_taxnode_into_list, convert_ncbirank_literal_to_integer,convert_ncbirank_integer_to_literal, curate_taxonomy
from mkbdr.edit_ncbi_taxdump import edit_ncbi_taxdump


class CurateTest(unittest.TestCase):
    """
    test case to check functions from the module curate
    """      
    
    def test_is_taxnode_into_list(self):
        """
        test case function is_taxnode_into_list        
        """
        listOfNodes = []
        node = TaxonNode(taxid = 2 , parent_taxid = 2, scientific_name = 'Testus testus', rank = 'genus')
        ## empty list
        assert is_taxnode_into_list(listOfNodes, node) is False
        listOfNodes = [node]
        ## the node is into the list
        assert is_taxnode_into_list(listOfNodes, node) is True
        node2 = TaxonNode(taxid = 3 , parent_taxid = 2, scientific_name = 'Testus tricki', rank = 'genus')
        ## the node is not into the list
        assert is_taxnode_into_list(listOfNodes, node2) is False
    
    def test_convert_ncbirank_literal_to_integer(self):
        """
        test case function convert_ncbirank_literal_to_integer
        """
        assert convert_ncbirank_literal_to_integer("species") == 0
        assert convert_ncbirank_literal_to_integer("genus") == 1
        assert convert_ncbirank_literal_to_integer("family") == 2

    def test_convert_ncbirank_integer_to_literal(self):
        """
        test case function convert_ncbirank_integer_to_literal
        """
        convert_ncbirank_integer_to_literal(0) == 'species'
        convert_ncbirank_integer_to_literal(1) == 'genus'
        convert_ncbirank_integer_to_literal(2) == 'family'


    def test_curate_taxonomy(self):
        """
        test case function curate_taxonomy
        """
        cureNames= ['specus' , 'genus', 'familus']
        ## species level        
        assert curate_taxonomy(2, 3, 0, cureNames, 0) == (0, 1, 'specus', 'genus', 0)
        ## genus level
        assert curate_taxonomy(2, 3, 1, cureNames, 0) == (0, 1, 'specus', 'genus', 1)
        ## family level
        assert curate_taxonomy(2, 3, 2, cureNames, 0) == (0, 1, 'specus', 'genus', 2)
        ## genus is unknown
        cureNames= ['NA' , 'NA', 'familus']
        assert curate_taxonomy(2, 3, 2, cureNames, 0) == (1, 2, 'NA', 'familus', 99)