import pandas
# bioinformatics
from ete3 import Tree, TreeStyle, NodeStyle, TextFace, NCBITaxa
import pytaxize
from pytaxize import gn
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
## local
from mkbdr.objects import Sample, DescriptionRecord
from mkbdr.validate import check_record_description, check_format_species_name, full_taxonomy_sample


def query_globalnames(speciesName):
    """
    Uses the Global Names Resolver to resolve scientific names
    If the query fails it returns False
    If the query success it returns a list of globalNames (each item is a database)
    """
    globalnamesQuery = gn.resolve(speciesName)
    if len(globalnamesQuery[0]) > 0:
        return globalnamesQuery[0]
    else:
        return False

def extract_classification_DB_globalnames(selectDB, globalNames):
    """
    From a globalNames query
    Get the results of the given database 'selectDB'
    Return a dictionnary with keys as rank and values as rank name
    return False if the database is not found in globalNames query
    """
    for gbn in globalNames:
        if str(selectDB) == str(gbn['data_source_title']):
            classificationPath = gbn['classification_path'].split('|')
            classificationPathRanks = gbn['classification_path_ranks'].split('|')
            zip_iterator = zip(classificationPathRanks, classificationPath)
            classificationDic = dict(zip_iterator)
            return classificationDic        
    return False


def ncbi_rank_synonyms(classificationDic, ncbi):
    """
    From a classification/taxonomy of an external DB given as a dictionnary
    with keys as rank and values as rank name
    Seek rank (genus, family) name match in NCBI taxonomy
    return a dictionnary with each NCBI rank as a key and name of the rank as values
    return False if no rank found
    """
    ncbiSynonymsRankDic = {}
    for rank in ['genus', 'family']:
            if rank in list(classificationDic.keys()):
                rankName = classificationDic[rank]
                rankNCBI = ncbi.get_name_translator([rankName])
                try:                
                    taxid = rankNCBI[rankName][0]
                except:
                    taxid = False
                if taxid is not False:
                    ncbiSynonymsRankDic[rank] = rankName
                else:
                    ncbiSynonymsRankDic[rank] = False
    if len(ncbiSynonymsRankDic) > 0:
        return ncbiSynonymsRankDic
    else:
        return False


def df_ncbi_rank_synonyms(oldSpeciesName, ncbiSynonymsRankDic, selectDB):
    """
    Generate dataframe for GlobalNames Rank taxonomy valided by NCBI taxonomy
    Args: current species name, Dictionnary with each NCBI rank as a key and name of the rank as values, Selected database name from GlobalNames
    """
    if ncbiSynonymsRankDic['genus'] is not False and ncbiSynonymsRankDic['family'] is not False:
        ncbiSynonymsRankData = {'current_name': oldSpeciesName, 'ncbi_name': 'NA', 'genus': ncbiSynonymsRankDic['genus'], 'family': ncbiSynonymsRankDic['family'], 'ncbi_rank': 'genus', 'method': selectDB}
    elif ncbiSynonymsRankDic['family'] is not False:
        ncbiSynonymsRankData = {'current_name': oldSpeciesName, 'ncbi_name': 'NA', 'genus': 'NA', 'family': ncbiSynonymsRankDic['family'], 'ncbi_rank': 'family', 'method': selectDB}
    else:
        ncbiSynonymsRankData = {'current_name': oldSpeciesName, 'ncbi_name': 'NA', 'genus': 'NA', 'family': 'NA', 'ncbi_rank': 'NA', 'method': selectDB}
    ncbiSynonymsRankDf = pandas.DataFrame([ncbiSynonymsRankData])
    return ncbiSynonymsRankDf


def ncbi_species_name_synonyms(speciesName, ncbi):
    """
    Given an inexact species name, returns the best match in the NCBI database of taxa names as a Sample class object
    Return false if no match found
    """
    ncbiBestSynonym = ncbi.get_fuzzy_name_translation(speciesName)
    taxid = ncbiBestSynonym[0]
    if taxid is not None:
        synonymSample = full_taxonomy_sample("synonymNCBI", taxid, ncbi, "NCBI synonym score="+str(ncbiBestSynonym[2]))              
        return synonymSample
    else:
        return False


def df_ncbi_species_name_synonyms(oldSpeciesName, synonymSample):
    """
    Generate dataframe for NCBI synonym
    Args: current species name, Sample Class Object with curated taxonomy information
    """
    ncbiSynonymSpeciesNameData = {'current_name': oldSpeciesName, 'ncbi_name': str(synonymSample.species_name), 'genus': str(synonymSample.genus_name), 'family': str(synonymSample.family_name), 'ncbi_rank':'species', 'method': str(synonymSample.context)}
    ncbiSynonymSpeciesNameDf = pandas.DataFrame([ncbiSynonymSpeciesNameData])
    return ncbiSynonymSpeciesNameDf


def df_failure_curation(currentSpeciesName, Failedmethod):
    """
    Generate a dataframe for failed curation from a given species name
    """
    failedCurationData = {'current_name': currentSpeciesName, 'ncbi_name': 'NA', 'genus': 'NA', 'family': 'NA', 'ncbi_rank': 'NA', 'method': Failedmethod}
    failedCurationDf = pandas.DataFrame([failedCurationData])
    return failedCurationDf


def generate_curation_df(faultyFastaFile, ncbi, databaseGlobalNames='FishBase Cache'):
    """
    Workflow
    1) Check fasta format
    2) check species name format
    2b) check species name is not already cured (remove doublon)
    3) Seek synonyms of species name in NCBI
    4) Seek globalnames of species name
    5) Select the database query 'databaseGlobalNames' from GlobalNames
    6) Extract taxonomy ranks from global names
    7) Select taxonomy ranks that match with NCBI taxonomy ranks
    8) Write a dataframe with header = current_name;ncbi_name;genus;family;method
    Return the curation dataframe
    """
    df = pandas.DataFrame(columns=['current_name', 'ncbi_name', 'genus', 'family', 'ncbi_rank', 'method'])  
    for record in SeqIO.parse(faultyFastaFile, "fasta"):
        checkedRecord = check_record_description(record.description)
        if checkedRecord is not False:
            checkedSpeciesName = check_format_species_name(checkedRecord.species_name)
            if checkedSpeciesName is not False:
                ##print(set(df['current_name']))              
                if str(checkedSpeciesName) not in set(df['current_name']) or df['current_name'].empty is True:
                    speciesNameSynonym = ncbi_species_name_synonyms(checkedSpeciesName, ncbi)
                    if speciesNameSynonym is not False:
                        speciesNameSynonymDf = df_ncbi_species_name_synonyms(checkedSpeciesName, speciesNameSynonym)
                        df = df.append(speciesNameSynonymDf, ignore_index=True)
                    else:
                        globalNames = query_globalnames(checkedSpeciesName)
                        if globalNames is not False:
                            classificationDic = extract_classification_DB_globalnames(databaseGlobalNames, globalNames)
                            if classificationDic is not False:
                                ncbiSynonymsRankDic = ncbi_rank_synonyms(classificationDic, ncbi)
                                if ncbiSynonymsRankDic is not False:
                                    ncbiSynonymsRankDf = df_ncbi_rank_synonyms(checkedSpeciesName, ncbiSynonymsRankDic, databaseGlobalNames)
                                    df = df.append(ncbiSynonymsRankDf, ignore_index=True)
                                else:
                                    failedMethod = 'FAILURE: '+str(databaseGlobalNames)+' no match in NCBI taxonomy at family rank'
                                    failedDf = df_failure_curation(checkedSpeciesName, failedMethod)
                                    df = df.append(failedDf, ignore_index=True)
                            else:
                                failedMethod = 'FAILURE: '+str(databaseGlobalNames)+' database not found in globalNames query'
                                failedDf = df_failure_curation(checkedSpeciesName, failedMethod)
                                df = df.append(failedDf, ignore_index=True)
                        else:
                            failedMethod = 'FAILURE: global Names Resolver failed'
                            failedDf = df_failure_curation(checkedSpeciesName, failedMethod)
                            df = df.append(failedDf, ignore_index=True)
                else:
                    print(checkedSpeciesName+" already cured.")                 
            else:
                failedMethod = 'FAILURE: faulty species name format'
                failedDf = df_failure_curation(checkedSpeciesName, failedMethod)
                df = df.append(failedDf, ignore_index=True)
        else:
            failedMethod = 'FAILURE: error fasta format'
            failedDf = df_failure_curation(checkedSpeciesName, failedMethod)
            df = df.append(failedDf, ignore_index=True)
    return df





