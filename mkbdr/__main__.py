#!/usr/bin/env python3
#===============================================================================
#INFORMATION
#===============================================================================
# Codes for mkbdr 
# 
# CEFE - EPHE - RESERVEBENEFIT 2021
# Guerin Pierre-Edouard
#
# mkbdr is a toolkit to build a custom metabarcoding reference database
# mkbdr is a python3 program.
#
# git repository : 
#
#==============================================================================
#MODULES
#==============================================================================

## Standard library imports
import re
import sys
import argparse
import csv
from os import path
import sys
import pandas

## Third party imports
from ete3 import Tree, TreeStyle, NodeStyle, TextFace, NCBITaxa
import numpy as np
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import pytaxize
from pytaxize import gn

## Local applications import
from mkbdr.arguments import parse_args
from mkbdr.load import load_taxdump, taxdump2dic
from mkbdr.validate import validate_fasta, report_validate_results
from mkbdr.curate import curation
from mkbdr.curegen import generate_curation_df
from mkbdr.init_ncbi_taxdump import download_ncbi_taxdump_archive

#==============================================================================
#MAIN
#==============================================================================
def main():
    args = parse_args()
    if args.command == 'validate':
        print('Validate records...')
        ncbi = load_taxdump(args.ncbi_taxdump_load)
        taxDic = taxdump2dic(ncbi, rootTaxon=args.root_ncbi_taxon_id)
        rawResults = validate_fasta(args.fasta, taxDic, ncbi)
        results = curation(args.curate, rawResults, taxDic, ncbi, args.ncbi_taxonomy_edition)
        SeqIO.write(results['valide'], str(args.output_prefix)+'_valide.fasta', 'fasta')
        SeqIO.write(results['faultyFormat'], str(args.output_prefix)+'_faulty_format.fasta', 'fasta')        
        SeqIO.write(results['faultyTaxon'], str(args.output_prefix)+'_faulty_taxon.fasta', 'fasta')
        print(report_validate_results(results))
    elif args.command == 'curegen':
        print('Curation generation...')
        ncbi = load_taxdump(args.ncbi_taxdump_load)
        taxDic = taxdump2dic(ncbi, rootTaxon=args.root_ncbi_taxon_id)
        curegenDf = generate_curation_df(args.fasta, ncbi, args.database_globalnames)
        print(curegenDf)
        curegenDf.to_csv(str(args.output_prefix)+'_curation.csv', index = None, sep = ';')
    elif args.command == 'init_ncbi_taxdump':
        print('Initialising ncbi taxdump...')
        download_ncbi_taxdump_archive(args.folder_path, args.decompress)
    else:
        sys.exit("ERROR: unknown command {0}.".format(args.command))


if __name__ == '__main__':
    main()
