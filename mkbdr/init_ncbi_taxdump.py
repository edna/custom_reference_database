from pathlib import Path
import os
import subprocess

## cd TAXO/testouille; tar zxvf taxdump_2021.tar.gz ; cd ../../

def makeDirectory(path_to_directory):
   p = Path(path_to_directory)
   p.mkdir(exist_ok=True, parents=True)


def is_empty_directory(path_to_directory):
    directory = os.listdir(path_to_directory) 
    if len(directory) == 0: 
        return True
    else:
        return False


def untar_ncbi_taxdump_archive(taxdumpArchivePath, taxdumpDir):
    subprocess.call(["tar", "zxvf", taxdumpArchivePath, "-C", taxdumpDir ])


def download_ncbi_taxdump_archive(taxdumpDir='.', decompress=False):    
    if os.path.exists(taxdumpDir):
        if not is_empty_directory:
            [f.unlink() for f in Path(taxdumpDir).glob("*") if f.is_file()]             
    ftp_path = 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz'
    taxdumpArchivePath = taxdumpDir+'/taxdump.tar.gz'
    makeDirectory(taxdumpDir)
    subprocess.call(["wget", "-O", taxdumpArchivePath, ftp_path])
    if decompress is True:
        print('Decompressing NCBI taxdump archive...', end='')
        untar_ncbi_taxdump_archive(taxdumpArchivePath, taxdumpDir)
        print('done.')
    print('NCBI taxdump initialisation successful !')
    print('Path: {0}'.format(taxdumpDir))


