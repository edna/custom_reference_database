import shutil
import copy
import os
import sys
## local
from mkbdr.objects import TaxonNode


def extract_dic_from_dmp(dmpFile):
    """
    read a .dmp file
    return a dictionnary with key as taxid and value as the corresponding line as a list
    (note: in nodes.dmp, each record consists of one or more fields delimited by tab, vertical bar, and tab
    records are delimited by tab, retourchariot endline )
    """
    dmpDic = {}
    with open(dmpFile, 'r') as reader:
        for ligne in reader.readlines():
            ligneSplit = ligne.split("\t|\t")
            taxid = int(ligneSplit[0])
            if taxid not in dmpDic: 
                dmpDic[taxid] = [ligneSplit]
            else:                
                dmpDic[taxid].append(ligneSplit)
        reader.close()
    return dmpDic


def add_newline_dmp(dmpDic, node, mode):
    """
    From a TaxonNode and nodes dictionnary objects
    seek node genus or family id in dmpDic
    then return the new line to add to nodes.dmp with the node information
    """
    try:
        dmpNewLigne = dmpDic[int(node.parent_taxid)][0].copy()
        dmpTaxid = node.parent_taxid
    except KeyError:
        print("FATAL ERROR: taxon {0} not in {1}.dmp".format(node.parent_taxid, mode))
    dmpNewLigne[0] = str(node.taxid)
    if mode == "names":
        dmpNewLigne[1] = str(node.scientific_name.replace('_',' '))
        dmpNewLigne[2] = ''
        dmpNewLigne[3] = 'scientific name'+'\t|\n'
    elif mode == "nodes":        
        dmpNewLigne[1] = str(dmpTaxid)
        dmpNewLigne[2] = str(node.rank)
        #dmpDic[dmpTaxid][0][11] = str(0)
    else:
        print("ERROR: {0} is unknown. Possible values are 'nodes' or 'names'".format(mode))
    dmpDic[str(node.taxid)] = [dmpNewLigne]
    return dmpDic


def write_dmp(dmpFile, dmpDic):
    """
    from dmp dictionnary object
    write a dmp file
    """
    with open(dmpFile, 'w') as newDmpFile:
        for key, val in dmpDic.items():
            for elem in val:          
                ligne = '\t|\t'.join(str(i) for i in elem)
                newDmpFile.write(ligne)
        newDmpFile.close()


def edit_ncbi_taxdump(listOfTaxNodes, ncbiTaxoDir):
    print('Editing ncbi_taxdump files...', end='')
    nodesFile=str(ncbiTaxoDir)+'/nodes.dmp'
    namesFile=str(ncbiTaxoDir)+'/names.dmp'
    if not os.path.isfile(nodesFile) or not os.path.isfile(namesFile):
        sys.exit('FATAL ERROR: {0} or {1} not found in taxdump directory {2}.'.format(nodesFile, namesFile, ncbiTaxoDir))
    ## back up
    #shutil.copyfile(nodesFile, nodesFile+".bak")
    #shutil.copyfile(namesFile, namesFile+".bak")
    nodesDic = extract_dic_from_dmp(nodesFile)
    namesDic = extract_dic_from_dmp(namesFile)
    newNodesDic = nodesDic
    newNamesDic = namesDic
    for node in listOfTaxNodes:
        newNodesDic = add_newline_dmp(newNodesDic, node, 'nodes')
        newNamesDic = add_newline_dmp(namesDic, node, 'names')
    write_dmp(nodesFile, newNodesDic)
    write_dmp(namesFile, newNamesDic)
    print('done.')
    print('{0} new nodes have been added in {1} and {2}.'.format( len(listOfTaxNodes), nodesFile, namesFile))
    return True
    

