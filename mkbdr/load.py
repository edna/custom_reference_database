from ete3 import Tree, TreeStyle, NodeStyle, TextFace, NCBITaxa
from os import path


def load_taxdump(localTaxdumpArchiveDir):
    """
    load taxonomy ncbi
    """    
    if localTaxdumpArchiveDir == "NA":
        print("Loading local NCBI taxonomy...", end='')
        ncbi = NCBITaxa()        
    else:
        print("Loading for the first time NCBI taxonomy from {0}...".format(localTaxdumpArchiveDir), end='')
        ncbi = NCBITaxa(taxdump_file=path.abspath(localTaxdumpArchiveDir))
    print("done.")
    return ncbi


def taxdump2dic(ncbiTaxaElem, rootTaxon=7742):
    """
    convert ncbiTaxa object into dictionnary with taxids and species names as keys and values
    """
    descTaxId=ncbiTaxaElem.get_descendant_taxa(rootTaxon, intermediate_nodes=True, rank_limit='species', collapse_subspecies=False)
    descNames=ncbiTaxaElem.translate_to_names(descTaxId)
    zip_iterator = zip(descTaxId, descNames)
    taxDic = dict(zip_iterator)
    return taxDic