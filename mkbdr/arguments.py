import argparse
import sys
import os
import pandas

from mkbdr import __version__


HELPER_TEXT ="""
.___  ___.  __  ___ .______    _______  .______      
|   \/   | |  |/  / |   _  \  |       \ |   _  \     
|  \  /  | |  '  /  |  |_)  | |  .--.  ||  |_)  |    
|  |\/|  | |    <   |   _  <  |  |  |  ||      /     
|  |  |  | |  .  \  |  |_)  | |  '--'  ||  |\  \----.
|__|  |__| |__|\__\ |______/  |_______/ | _| `._____|
                                                     
_______________________________________________________________________________


Pierre-Edouard GUERIN, Laetitia MATHON, Virginie MARQUES, Stephanie MANEL
CNRS, EPHE, Sorbonne University, Montpellier, France
version {0} March 2021
Usage:
> mkbdr [options]
For help:
> mkbdr --help
""".format(__version__)


def check_taxdumpdir(ncbiTaxdumpDir):
    nodesFile=str(ncbiTaxdumpDir)+'/nodes.dmp'
    namesFile=str(ncbiTaxdumpDir)+'/names.dmp'
    if not os.path.exists(ncbiTaxdumpDir):
        print('ARGUMENTS ERROR: arguments ncbi_taxdump {0}. This path not exists.'.format(ncbiTaxdumpDir))
        return False
    if not os.path.isfile(nodesFile):
        print('ARGUMENTS ERROR: arguments ncbi_taxdump {0}. {1} is missing.'.format(ncbiTaxdumpDir, nodesFile))
        return False
    elif not os.path.isfile(namesFile):
        print('ARGUMENTS ERROR: arguments ncbi_taxdump {0}. {1} is missing.'.format(ncbiTaxdumpDir, namesFile))
        return False
    else:
        return True


def check_curatecsv(curateCsvFile):
    curateColons = ['current_name', 'ncbi_name', 'genus', 'family', 'ncbi_rank'] 
    if not os.path.isfile(curateCsvFile):
        print('ARGUMENTS ERROR: arguments curate {0}. {0} file not exists.'.format(curateCsvFile))
        return False
    try:
        dfCure = pandas.read_csv(curateCsvFile, sep=";")        
    except ValueError:
        print("ARGUMENTS ERROR: arguments curate {0}. {0} is not a CSV file and must have ';' as delimiter'.".format(curateCsvFile))
        return False
    colons = list(dfCure.columns.values)
    for colon in curateColons:
        if colon not in colons:
            print("ARGUMENTS ERROR: arguments curate {0}. {1} colon is missing.".format(curateCsvFile, colon))
            return False
    return True
    

def parse_args(usage=HELPER_TEXT):
    parser = argparse.ArgumentParser(description='mkbdr - to build a custom metabarcoding reference database.')
    parser.add_argument("-v","--version", action="version", help="Show version number and exit", version=__version__)

    subprasers = parser.add_subparsers(dest='command')

    validate = subprasers.add_parser('validate', help='check format and taxonomy')
    validate.add_argument("-f","--fasta", type=str, help='path of the barcodes sequences FASTA file', required=True)
    validate.add_argument("-c","--curate", type=str, help='path of the taxonomy curation CSV file. Header must be current_name;ncbi_name;genus;family;ncbi_rank. A curation CSV file can be generated with the command curegen', required=False, default="NA")
    validate.add_argument("-l","--ncbi_taxdump_load",  type=str, default='NA', help='load NCBI taxonomy archive')
    validate.add_argument("-e","--ncbi_taxonomy_edition", type=str, default='NA', help='Folder of NCBI taxonomy files to edit in order to add new taxonomy nodes')
    validate.add_argument("-o","--output_prefix", type=str, help='prefix of the output FASTA such as [PREFIX].fasta')
    validate.add_argument("-r", "--root_ncbi_taxon_id", type=int, help='taxon ID of the root of the NCBI taxonomy tree to load', default=7742)

    curegen = subprasers.add_parser('curegen', help='generate curation file to correct wrong taxonomy')
    curegen.add_argument("-f","--fasta", type=str, help='path of the barcodes sequences FASTA file', required=True)
    curegen.add_argument("-d","--database_globalnames", type=str, help="Name of the selected database from GlobalNames i.e. 'Catalogue of Life' or 'Fishbase Cache'", required=False, default='FishBase Cache')
    curegen.add_argument("-o","--output_prefix", type=str, help='prefix of the output curated taxonomy CSV such as [PREFIX].csv')
    curegen.add_argument("-l","--ncbi_taxdump_load",  type=str, default='NA', help='load NCBI taxonomy archive')
    curegen.add_argument("-r", "--root_ncbi_taxon_id", type=int, help='taxon ID of the root of the NCBI taxonomy tree to load', default=7742)


    init_ncbi_taxdump = subprasers.add_parser('init_ncbi_taxdump', help='download the NCBI taxdump archive')
    init_ncbi_taxdump.add_argument("-f", "--folder_path", type=str, help='Folder path where to download the NCBI taxdump archive', required=False, default='.')
    init_ncbi_taxdump.add_argument("-d", "--decompress", action='store_true', help='Decompress the archive')

    args = parser.parse_args()

    if args.command not in ['validate', 'curegen', 'init_ncbi_taxdump']:
        print(usage)
        sys.exit(0)
    print("Checking arguments...", end='')
    ## check arguments are not faulty
    if args.command == 'validate':
        if args.curate != 'NA':
            if check_curatecsv(args.curate) is False:
                print(usage)
                sys.exit(0)
        if args.ncbi_taxonomy_edition !='NA':
            if check_taxdumpdir(args.ncbi_taxonomy_edition) is False:
                print(usage)
                sys.exit(0)
    print("done.")
    return args
