## base
import re
import sys
import argparse
import csv
from os import path
import sys
## external
from ete3 import Tree, TreeStyle, NodeStyle, TextFace, NCBITaxa
import numpy as np
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
## local
from mkbdr.objects import Sample, DescriptionRecord

def check_record_description(record_description):
    '''
    Check fasta record description format
    accepted format is
    > sampleid ; species_name=Genus species
    ATTATG
    return False when the format is faulty
    return DescriptionRecord object when the format is ok
    '''
    splitted = record_description.split(';')
    if len(splitted) > 1:
        idSample = splitted[0]
        description = splitted[1]
        if '=' in idSample:
            ## L'ID de la sequence ne doit pas comporter de caractere '=' car c'est attendu dans la description
            return False
        else:
            if "species_name=" in description:
                speciesName = str(description.split('=')[1])
                descriptionRecord = DescriptionRecord(sampleid=idSample, species_name=speciesName)
                return descriptionRecord
            else:
                ## no species_name element in description
                return False
    else:
        ## the header doesn't have at least 2 colons separeted by ;
        return False


def check_format_species_name(species_name):
    '''
    Check species name format
    Excepted format "Genus species" or "Genus_species"
    '''
    patternSpeciesPerfect = "^[A-Z][a-z]+ [a-z]+$"
    if re.match(patternSpeciesPerfect, species_name):
        return species_name
    elif re.match(r"^[A-Z][a-z]+\_[a-z]+$", species_name):
        return species_name.replace('_', ' ')
    else:
        return False


def check_dna_sequence(dna_sequence, code="ATGC"):
    '''
    Check DNA sequence format (iuapc ambiguity, gaps)
    '''
    for base in dna_sequence:
        if base not in code:
            return False
    return True


def check_taxonomy_species_name(taxon_name, reference, ncbi, selectedRank="species"):
    """
    Seek taxon name in NCBI taxonomy reference items list
    return False if not found
    return the matching NCBI taxonomy if found
    """
    matching = { k:v for k,v in reference.items() if taxon_name == v }
    if len(matching) == 1:
        rank = list(ncbi.get_rank(list(matching.keys())).values())[0]
        if rank == selectedRank:
            return matching
        else:
            ## the rank in not "species"
            return False
    elif len(matching) > 1:
        ## they are many matches so the species name is ambiguous
        return False
    else:
        ## this species is unknown in ncbi
        return False


def full_taxonomy_sample(sampleid, taxonid, ncbi, sampleContext):
    """
    Args: id of the record, taxonid, ncbitaxa object, context information
    Check that the taxonid is in ncbitaxa and get genus and family lineage taxid
    return a Sample object class
    """
    lineage=ncbi.get_lineage(taxonid)
    if len(lineage) > 1:
        ranks=ncbi.get_rank(lineage)
        if len(ranks) > 1:
            idGenus = False
            idFamily = False
            for k, v in ranks.items():
                if v == "genus":
                    idGenus = k
                if v == "family":
                    idFamily = k
                if idGenus and idFamily:
                    break
            if idGenus is False or idFamily is False:
                ## Genus or Family is unknown in ncbi
                return False
            else:
                genusName = str(ncbi.translate_to_names([idGenus])[0])
                familyName = str(ncbi.translate_to_names([idFamily])[0])
                speciesName = str(ncbi.translate_to_names([taxonid])[0])
                sample = Sample(sampleid=sampleid,
                                species_name=speciesName,
                                genus_taxid=str(idGenus),
                                genus_name=genusName,
                                family_taxid=str(idFamily),
                                family_name = familyName,
                                scientific_name = speciesName,
                                taxid = taxonid,
                                rank = "species",
                                context = sampleContext)
                return sample
        else:
            ## The taxon taxonomy is unknown or empty
            return False
    else:
        ## This taxon have no taxonomy in ncbi
        return False


def validate_fasta(rawFastaFile, taxDic, ncbi):
    """
    main workflow
    For each records in raw fasta file:
    1) check format fasta
    2) check format species name
    3) check DNA sequence
    4) Check NCBI taxonomy of species name
    5) Generate a Sample Object Class with all taxids
    6) Generate SeqRecord Object Class

    Records which fail a check are get into results['faulty*']
    Records which success all the checks are get into results['valide']
    Return results dictionnary
    """
    results = {}
    results['valide'] = []
    results['faultyFormat'] = []
    results['faultyTaxon'] = []
    for record in SeqIO.parse(rawFastaFile, "fasta"):
        #print(record.id)
        #print(record.description)
        #print(record.seq)
        checkedRecord = check_record_description(record.description)
        if checkedRecord is not False:
            checkedFormat = check_format_species_name(checkedRecord.species_name)
            if checkedFormat is not False:
                if check_dna_sequence(record.seq):
                    checkedRecord.species_name = checkedFormat
                    checkedTaxon = check_taxonomy_species_name(checkedRecord.species_name, taxDic, ncbi)
                    if checkedTaxon is not False:
                        #print(checkedTaxon)
                        sample = full_taxonomy_sample(checkedRecord.sampleid , list(checkedTaxon.keys())[0], ncbi, "valid")
                        if sample is not False:
                            thisValideRecord = SeqRecord(id = sample.sampleid, description = sample.description(), seq = record.seq)
                            results["valide"].append(thisValideRecord)
                        else:
                            ## faulty rank taxonomy
                            faultydescription = str(record.description)+'; faulty rank taxonomy: family or genera not found'
                            thisFaultyRecord = SeqRecord(id = record.id, description = faultydescription, seq=record.seq)
                            results["faultyTaxon"].append(thisFaultyRecord)
                    else:
                        ## faulty taxonomy
                        faultydescription = str(record.description)+'; faulty taxonomy: species name '+ str(checkedRecord.species_name)+ ' not found in NCBI'
                        thisFaultyRecord = SeqRecord(id = record.id, description = faultydescription, seq=record.seq)
                        results["faultyTaxon"].append(thisFaultyRecord)
                else:
                    faultydescription = str(record.description)+' ; faulty DNA sequence'
                    thisFaultyRecord = SeqRecord(id = record.id, description = faultydescription, seq=record.seq)
                    results["faultyFormat"].append(thisFaultyRecord)
            else:
                ## faulty species name format
                faultydescription = str(record.description)+' ; faulty species name format ' + str(checkedRecord.species_name)
                thisFaultyRecord = SeqRecord(id = record.id, description = faultydescription, seq=record.seq)
                results["faultyFormat"].append(thisFaultyRecord)
        else:
            ## faulty record description format
            print("ERROR FORMAT FASTA {0}: {1} {2}.\n Excepted format is\n\n>sampleid ; species_names=Genus Species\nACTAG\n".format(rawFastaFile,record.id, record.description))
    return results


def report_validate_results(results):
    nValide=len(results['valide'])
    nFaultyFormat=len(results['faultyFormat'])
    nFaultyTaxon=len(results['faultyTaxon'])
    nRecords=nValide+nFaultyFormat+nFaultyTaxon
    reportstring ="{0} processed records.\nOn these records, {1} are valid, {2} are faulty format and {3} are faulty taxon.".format(nRecords, nValide,nFaultyFormat, nFaultyTaxon)
    return reportstring
