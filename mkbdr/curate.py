## Standard library imports
import pandas
import sys
import itertools
import copy
import re

## Third party imports
from ete3 import Tree, TreeStyle, NodeStyle, TextFace, NCBITaxa
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

## Local applications import
from mkbdr.objects import GenID, Sample, TaxonNode
from mkbdr.validate import check_taxonomy_species_name, full_taxonomy_sample
from mkbdr.edit_ncbi_taxdump import edit_ncbi_taxdump


def extract_species_name_from_description(description):
    return description.split(';')[1].split('=')[1].replace('_',' ')


def query_record_curation(description, dfCure):
    currentName = extract_species_name_from_description(description)
    cure = dfCure[dfCure['current_name'] == currentName]
    if cure.size == 0:
        return False
    else:
        return cure


def new_full_taxonomy_sample(cureNames, reference, ncbi, depth):
    ranks = ['family', 'genus']
    names = {'species':cureNames[0], 'genus':cureNames[1],'family':cureNames[2]}
    genID_species = GenID()
    sample = Sample(sampleid="CUSTOM"+str(genID_species.id),
    species_name=names['species'],
    genus_taxid='NA',
    genus_name='NA',
    family_taxid='NA',
    family_name = 'NA',
    scientific_name = names['species'],
    taxid = genID_species.taxid,
    rank = 'species',
    context = "new taxon node")
    for rank in ranks:
        if sample.family_taxid == 'NA':
            checkedTaxon = check_taxonomy_species_name(names[rank], reference, ncbi, rank)
            if checkedTaxon is not False:
                sample.family_taxid = str(list(checkedTaxon.keys())[0])
                sample.family_name = list(checkedTaxon.values())[0]
            else:
                print("ERROR: wrong ncbi taxon not found {0} {1}".format(rank, names[rank]))
                return False
        else:
            if depth == 1:
                checkedTaxon = check_taxonomy_species_name(names[rank], reference, ncbi, rank)
                if checkedTaxon is not False:
                    sample.genus_taxid = str(list(checkedTaxon.keys())[0])
                    sample.genus_name = list(checkedTaxon.values())[0]
                else:
                    print("ERROR: wrong ncbi taxon not found {0} {1}".format(rank, names[rank]))
                    return False
            elif depth == 2:
                genID_genus = GenID()
                sample.genus_taxid = genID_genus.taxid
                sample.genus_name = names[rank]
            else:
                print("ERROR: depth is not 1 or 2")
                return False
    return sample


def unique_sample_taxid(sample, listOfSamples):
    newSample = sample
    for s in listOfSamples:
        if str(sample.species_name) == str(s.species_name):
            newSample.taxid = s.taxid
            return newSample
    return sample





def convert_ncbirank_literal_to_integer(ncbirank):
    if ncbirank == 'species':
        return 0
    elif ncbirank == 'genus':
        return 1
    elif ncbirank == 'family':
        return 2
    else:
        sys.exit("ERROR: ncbi_rank literal value must be species|genus|family and not {0}".format(ncbirank))


def convert_ncbirank_integer_to_literal(ncbirank):
    if ncbirank == 0:
        return 'species'
    elif ncbirank == 1:
        return 'genus'
    elif ncbirank == 2:
        return 'family'
    else:
        sys.exit("ERROR: ncbi_rank integer value must be 0|1|2 and not {0}".format(ncbirank))


def curation_names(cure):
    cureNames= ['NA' , 'NA', 'NA']
    if cure.ncbi_name.values[0] == 'NA':
        if cure.current_name.values[0] != 'NA':
            cureNames[0] = cure.current_name.values[0]
        else:
            cureNames[0] = 'NA'
    else:
        cureNames[0] = cure.ncbi_name.values[0]
    cureNames[1] = cure.genus.values[0]
    cureNames[2] = cure.family.values[0]
    return cureNames


def curate_taxonomy(rank, parentRank, ncbi_rank, names, depth):
    if rank >= ncbi_rank:
        if names[rank] == 'NA':
            sys.exit("ERROR: ")
        else:
            if parentRank == 1:
                return (rank, parentRank, names[rank], names[parentRank], 0)
            else:
                return curate_taxonomy(rank-1, rank, ncbi_rank, names, depth)
    else:
        if names[rank] == 'NA':
            ## add parentRank and rank is unknown
            return (rank, parentRank, names[rank], names[parentRank], 99)
        else:
            ##addnodes
            depth+=1
            if parentRank == 1:
                return (rank, parentRank, names[rank], names[parentRank], depth)
            else:
                return curate_taxonomy(rank-1, rank, ncbi_rank, names, depth)


def is_taxnode_into_list(listOfTaxNodes, node):
    """
    Check if a TaxNode is already in a list of TaxNode objects
    """
    is_node_in_list=False
    for oldnode in listOfTaxNodes:
        if int(oldnode.taxid) == int(node.taxid):
            is_node_in_list = True
            break
    return is_node_in_list


def curation(curateCsvFile, results, taxDic, ncbi, ncbi_taxonomy_edition='NA'):
    if curateCsvFile == "NA":
        return results
    print("Curating records with faulty taxonomy...")
    curatedRecords = []
    curatedRecordsIds = []
    listOfTaxNodes = [[] for _ in range(2)]
    listOfSamples = []
    dfCure = pandas.read_csv(curateCsvFile,sep=";")
    dfCure['current_name'] = dfCure['current_name'].apply(lambda x: x.replace('_', ' ') if type(x) is str else x )
    dfCure['ncbi_name'] = dfCure['ncbi_name'].apply(lambda x: x.replace('_', ' ') if type(x) is str else x )
    dfCure = dfCure.fillna('NA')
    #print(dfCure)
    for record in results['faultyTaxon']:
        cureRecord = query_record_curation(record.description, dfCure)
        cureRankNCBI = convert_ncbirank_literal_to_integer(cureRecord.ncbi_rank.values[0])
        cureNames = curation_names(cureRecord)
        cureTaxonomy = curate_taxonomy(2, 3, cureRankNCBI, cureNames, 0)
        curatedName = cureTaxonomy[2]
        cureStatut = int(cureTaxonomy[4])
        #print(cureRecord)
        if cureStatut == 0:
            checkedTaxon = check_taxonomy_species_name(curatedName, taxDic, ncbi)
            if checkedTaxon is False:
                print("error "+curatedName)
            else:
                sample = full_taxonomy_sample(record.id , list(checkedTaxon.keys())[0], ncbi, 'curated')
                thisCuratedRecord = SeqRecord(id = re.sub(';', '', record.id), description = sample.description(), seq = record.seq)
                curatedRecordsIds.append(record.id)
                curatedRecords.append(thisCuratedRecord)
        elif ncbi_taxonomy_edition != 'NA':
            if cureStatut == 1 or cureStatut == 2:
                sample = new_full_taxonomy_sample(cureNames, taxDic, ncbi, cureStatut)
                sample = unique_sample_taxid(sample, listOfSamples)
                listOfSamples.append(sample)
                thisCuratedRecord = SeqRecord(id = re.sub(';', '', record.id), description = sample.description(), seq = record.seq)
                curatedRecordsIds.append(record.id)
                curatedRecords.append(thisCuratedRecord)
                if cureStatut == 2:
                    node = TaxonNode(taxid = sample.genus_taxid, parent_taxid = sample.family_taxid ,scientific_name = sample.genus_name, rank = 'genus')
                    if is_taxnode_into_list(listOfTaxNodes[0], node) is False:
                        listOfTaxNodes[0].append(node)
                node = TaxonNode(taxid = sample.taxid , parent_taxid = sample.genus_taxid, scientific_name = sample.scientific_name, rank = sample.rank)
                if is_taxnode_into_list(listOfTaxNodes[1], node) is False:
                    listOfTaxNodes[1].append(node)
            elif cureStatut == 99:
                print('Incomplete taxonomy: {0}'.format(str(record.id)))
            else:
                print('cureStatut is wrong: {0}'.format(str(record.id)))
        else:
            print('curation not performed on record {0}'.format(str(record.id)))
        #print(cureTaxonomy)
    ## update results records lists
    for listOfTaxNodesDepth in listOfTaxNodes:
        if len(listOfTaxNodesDepth) > 0:
            edit_ncbi_taxdump(listOfTaxNodesDepth, ncbi_taxonomy_edition)
    for record in curatedRecords:
        results['valide'].append(record)
    print("On {0} faulty records, {1} records are curated.".format(len(results['faultyTaxon']), len(curatedRecordsIds)))
    oldFaultyTaxon = results['faultyTaxon'].copy()
    results['faultyTaxon'] = []
    for record in oldFaultyTaxon:
        if record.id not in curatedRecordsIds:
            results['faultyTaxon'].append(record)
    return results

