import itertools

class GenID:
    id_iter = itertools.count()
    def __init__(self):
        self.id = next(GenID.id_iter)
        self.taxid = self.id+10000000


class Sample:
    def __init__(self, sampleid, species_name, genus_taxid, genus_name, family_taxid, family_name, scientific_name, taxid, rank, context):
        self.sampleid=sampleid
        self.species_name=species_name
        self.genus_taxid=genus_taxid
        self.genus_name=genus_name
        self.family_taxid=family_taxid
        self.family_name=family_name
        self.scientific_name=scientific_name
        self.taxid=taxid
        self.rank=rank
        self.context = context
    def description(self):
        return "taxid={6}; species_name={0}; genus_taxid={8}; genus_name={1}; family_taxid={2}; family_name={3}; scientific_name={4}; rank={5}; {7}".format(self.species_name, self.genus_name, self.family_taxid, self.family_name, self.scientific_name, self.rank, self.taxid, self.context, self.genus_taxid)

class DescriptionRecord:
    def __init__(self, sampleid, species_name):
        self.sampleid=sampleid
        self.species_name = species_name
    def __eq__(self, other):
        return self.sampleid == other.sampleid and self.species_name == other.species_name


class TaxonNode:
    def __init__(self, taxid, parent_taxid, scientific_name, rank):
        self.taxid=taxid
        self.parent_taxid=parent_taxid
        self.scientific_name=scientific_name
        self.rank=rank
      